# Toy robot

Solution is built following Clean Architecture and CQRS. It contains, the domain, api, ui, db migration and db.

1. Domain - Business logic, contains models, commands and queries.
2. api - Graphql server built on nestjs + Nestjs + fastify + mercurius
3. ui - UI built on Nextjs + genql + tailwindcss
4. db migration - Keep database schema and data up to data, using typeorm

## To run whole solution

```
npm run docker:up
```

## To stop running solution

```
npm run docker:down
```

## To setup local

```
npm run setup
```

## To run test

```
npm run test
```

## Notes

Solution is built with:
Node: v16.14.2
Docker: 4.2.0
