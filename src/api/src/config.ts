import { Config } from '@domain/models/common/config';

export const config: Config = {
  game: {
    boardX: 5,
    boardY: 5,
  },
  port: process.env.port || 5100,
  host: process.env.address || '0.0.0.0',
  corsOrigins: ['http://localhost:5001'],
  db: {
    type: 'postgres',
    url:
      process.env.DB_URL ||
      'postgres://postgres:db_password@localhost:5432/toy_robot',
    logging: 'all',
  },
};
