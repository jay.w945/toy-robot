import { Module } from '@nestjs/common';

import {
  DataSourceProvider,
  DATA_SOURCE,
} from '@domain/data/data-source.provider';
import { GAME_BOARD, Board } from '@domain/models/board.model';
import { CommandProcessor } from '@domain/comands/commandProcessor';
import { RobotQuery } from '@domain/queries/robotQuery';

import { config } from '../config';
import { GameQueryResolver } from './game.query.resolver';
import { GameMutationResolver } from './game.mutation.resolver';

@Module({
  providers: [
    {
      provide: DATA_SOURCE,
      useFactory: async () => {
        const dataSourceProvider = new DataSourceProvider(config);
        await dataSourceProvider.init();
        return dataSourceProvider.getAppDataSource();
      },
    },
    {
      provide: GAME_BOARD,
      useFactory: () => new Board(config.game.boardX, config.game.boardY),
    },
    CommandProcessor,
    RobotQuery,
    GameQueryResolver,
    GameMutationResolver,
  ],
})
export class GameModule {}
