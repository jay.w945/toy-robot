import { Args, Mutation, Resolver } from '@nestjs/graphql';

import { Robot } from '@domain/models/entities/robot.entity';
import { CommandProcessor } from '@domain/comands/commandProcessor';
import { CommandResult } from '@domain/models/command.model';

@Resolver(() => Robot)
export class GameMutationResolver {
  constructor(private readonly commandProcessor: CommandProcessor) {}

  @Mutation(() => CommandResult)
  async runCommand(@Args('command') command: string): Promise<CommandResult> {
    await this.commandProcessor.init();
    const result = await this.commandProcessor.process(command);
    return result;
  }
}
