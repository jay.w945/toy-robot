import { Args, Query, Resolver } from '@nestjs/graphql';

import { Robot } from '@domain/models/entities/robot.entity';
import { RobotQuery } from '@domain/queries/robotQuery';
import { Board } from '@domain/models/board.model';

import { config } from '../config';

@Resolver(() => Robot)
export class GameQueryResolver {
  constructor(private readonly robotQuery: RobotQuery) {}

  @Query(() => Robot, { nullable: true })
  async robot(
    @Args('id', { nullable: true }) id: string | null,
  ): Promise<Robot> {
    const robot = this.robotQuery.getRobot(id);
    return robot;
  }

  @Query(() => Board)
  board(): Board {
    return new Board(config.game.boardX, config.game.boardY);
  }
}
