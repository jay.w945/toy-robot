# Data Migrations

## To generate new migration

```
npm run migration:generate src/migrations/{NEW_MIGRATION_NAME}
```

## To run migration

```
npm run migration:run
```
