export const config = {
  type: "postgres",
  url:
    process.env.DB_URL ||
    "postgres://postgres:db_password@localhost:5432/toy_robot",
  logging: "all",
  synchronize: false,
  entities: ["../domain/src/models/entities/**/*.ts"],
  migrations: ["src/migrations/**/*.ts"],
  cli: {
    entitiesDir: "../domain/src/models/entities/**/*.ts",
    migrationsDir: "src/migrations",
  },
};
