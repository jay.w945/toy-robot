import "reflect-metadata";

import { DataSourceProvider } from "@domain/data/data-source.provider";

import { config } from "./config";

const appDataSourceProvider = new DataSourceProvider(config);
export const AppDataSource = appDataSourceProvider.getAppDataSource();
