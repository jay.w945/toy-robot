import { AppDataSource } from "./data-source";

const runMigrations = async (): Promise<void> => {
  await AppDataSource.initialize();
  await AppDataSource.runMigrations();
  await AppDataSource.destroy();
};

runMigrations();
