import { MigrationInterface, QueryRunner } from "typeorm";

export class init1650724293716 implements MigrationInterface {
    name = 'init1650724293716'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "command_logs" ("id" character varying NOT NULL, "timestamp" TIMESTAMP NOT NULL DEFAULT now(), "raw_command" character varying NOT NULL, "valid" boolean NOT NULL, CONSTRAINT "PK_15837db71653337438e0c578f56" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "robot" ("id" character varying NOT NULL, "timestamp" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL, "direction" character varying NOT NULL, "positionX" integer NOT NULL, "positionY" integer NOT NULL, "commandLogId" character varying, CONSTRAINT "UQ_37868a14509d18a0ceca3c886bc" UNIQUE ("id", "version"), CONSTRAINT "REL_9e9a9729489199d9af221c0dbf" UNIQUE ("commandLogId"), CONSTRAINT "PK_37868a14509d18a0ceca3c886bc" PRIMARY KEY ("id", "version"))`);
        await queryRunner.query(`ALTER TABLE "robot" ADD CONSTRAINT "FK_9e9a9729489199d9af221c0dbf6" FOREIGN KEY ("commandLogId") REFERENCES "command_logs"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "robot" DROP CONSTRAINT "FK_9e9a9729489199d9af221c0dbf6"`);
        await queryRunner.query(`DROP TABLE "robot"`);
        await queryRunner.query(`DROP TABLE "command_logs"`);
    }

}
