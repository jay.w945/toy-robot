import { Board } from "../models/board.model";
import { Direction } from "../models/direction.model";
import { Robot } from "../models/entities/robot.entity";
import { CommandDispatcher } from "./commandDispatcher";

describe("Verify commandDispatcher dispatch functionality", () => {
  const commandDispatcher = new CommandDispatcher();
  const board = new Board(5, 5);
  const robot1 = new Robot();
  robot1.direction = Direction.EAST;
  robot1.positionX = 4;
  robot1.positionY = 2;

  it("valid raw command, should return robot", async () => {
    const rawCommand = "PLACE 2,3,NORTH";

    const actual = commandDispatcher.dispatch(rawCommand, board, robot1);

    expect(actual.robot!.positionX).toBe(2);
    expect(actual.robot!.positionY).toBe(3);
    expect(actual.robot!.direction).toBe(Direction.NORTH);
  });

  it("invalid place command when there is no robot, should return null", async () => {
    const rawCommand = "PLACE 5,3,NORTH";

    const actual = commandDispatcher.dispatch(rawCommand, board, null);

    expect(actual.robot).toBeNull();
  });

  it("invalid place command when there is robot, should return previous robot", async () => {
    const rawCommand = "PLACE 5,3,NORTH";

    const actual = commandDispatcher.dispatch(rawCommand, board, robot1);

    expect(actual.robot!.direction).toBe(Direction.EAST);
    expect(actual.robot!.positionX).toBe(4);
    expect(actual.robot!.positionY).toBe(2);
  });

  it("invalid move command, should return previous robot", async () => {
    const rawCommand = "MOVE";

    const actual = commandDispatcher.dispatch(rawCommand, board, robot1);

    expect(actual.robot!.direction).toBe(Direction.EAST);
    expect(actual.robot!.positionX).toBe(4);
    expect(actual.robot!.positionY).toBe(2);
  });

  const noRobotCommandsTheory = ["MOVE", "LEFT", "RIGHT", "REPORT"];
  it.each(noRobotCommandsTheory)(
    "when there is no Robot, should return null",
    (command) => {
      const actual = commandDispatcher.dispatch(command, board, null);
      expect(actual.robot).toBeNull();
    }
  );

  const validCommandTheory = [
    {
      command: "PLACE 2,3,NORTH",
      direction: Direction.EAST,
      positionX: 0,
      positionY: 0,
      newDirection: Direction.NORTH,
      newX: 2,
      newY: 3,
    },
    {
      command: "LEFT",
      direction: Direction.EAST,
      positionX: 3,
      positionY: 2,
      newDirection: Direction.NORTH,
      newX: 3,
      newY: 2,
    },
    {
      command: "RIGHT",
      direction: Direction.SOUTH,
      positionX: 3,
      positionY: 2,
      newDirection: Direction.WEST,
      newX: 3,
      newY: 2,
    },
    {
      command: "MOVE",
      direction: Direction.EAST,
      positionX: 3,
      positionY: 3,
      newDirection: Direction.EAST,
      newX: 4,
      newY: 3,
    },
    {
      command: "REPORT",
      direction: Direction.WEST,
      positionX: 2,
      positionY: 3,
      newDirection: Direction.WEST,
      newX: 2,
      newY: 3,
    },
  ];
  it.each(validCommandTheory)(
    "with valid command, shoul work",
    ({
      command,
      direction,
      positionX,
      positionY,
      newDirection,
      newX,
      newY,
    }) => {
      const robot = new Robot();
      robot.direction = direction;
      robot.positionX = positionX;
      robot.positionY = positionY;

      const actual = commandDispatcher.dispatch(command, board, robot);

      expect(actual.robot!.direction).toBe(newDirection);
      expect(actual.robot!.positionX).toBe(newX);
      expect(actual.robot!.positionY).toBe(newY);
    }
  );
});
