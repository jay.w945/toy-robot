import { Board } from "../models/board.model";
import { Command, CommandResult, CommandType } from "../models/command.model";
import { Robot } from "../models/entities/robot.entity";
import { InvalidCommand } from "../models/invalidCommand.model";
import { CommandHandler } from "./handlers/commandHandler";
import { LeftCommandHandler } from "./handlers/leftCommandHandler";
import { MoveCommandHandler } from "./handlers/moveCommandHandler";
import { PlaceCommandHandler } from "./handlers/placeCommandHandler";
import { ReportCommandHandler } from "./handlers/reportCommandHandler";
import { RightCommandHandler } from "./handlers/rightCommandHandler";

const handlerMap: { [key: string]: CommandHandler<Command> } = {
  [CommandType.Left]: new LeftCommandHandler(),
  [CommandType.Right]: new RightCommandHandler(),
  [CommandType.Move]: new MoveCommandHandler(),
  [CommandType.Place]: new PlaceCommandHandler(),
  [CommandType.Report]: new ReportCommandHandler(),
};

export class CommandDispatcher {
  dispatch(
    rawCommand: string,
    board: Board,
    robot: Robot | null
  ): CommandResult {
    let invalidCommand = new InvalidCommand();
    let parts = rawCommand
      .trim()
      .toUpperCase()
      .split(/[\s|,]/)
      .filter((v) => v);

    if (parts.length <= 0 || !handlerMap[parts[0]]) {
      return new CommandResult(invalidCommand, robot);
    }

    const commandHandler = handlerMap[parts[0]];
    return commandHandler.procecss(rawCommand, board, robot);
  }
}
