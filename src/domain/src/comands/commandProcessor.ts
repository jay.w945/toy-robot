import { Inject, Injectable } from "@nestjs/common";
import { DataSource, FindManyOptions, Repository } from "typeorm";
import { CommandDispatcher } from "./commandDispatcher";

import { DATA_SOURCE } from "../data/data-source.provider";
import { Board, GAME_BOARD } from "../models/board.model";
import { CommandResult } from "../models/command.model";
import { CommandLogs } from "../models/entities/commandLogs.entity";
import { Robot } from "../models/entities/robot.entity";

@Injectable()
export class CommandProcessor {
  private inited: boolean = false;
  private robot: Robot | null = null;
  private repo: Repository<Robot>;
  private commandProcessor!: CommandDispatcher;

  constructor(
    @Inject(DATA_SOURCE) private dataSource: DataSource,
    @Inject(GAME_BOARD) private board: Board
  ) {
    this.repo = this.dataSource.getRepository(Robot);
    this.commandProcessor = new CommandDispatcher();
  }

  getRobot(): Robot | null {
    return this.robot;
  }

  async init() {
    if (this.inited) {
      return;
    }

    await this.loadRobot();
    this.inited = true;
  }

  async loadRobot() {
    const query = {
      order: { timestamp: "DESC" },
      take: 1,
    } as FindManyOptions<Robot>;

    this.robot && (query.where = { id: this.robot.id });
    const results = await this.repo.find(query);

    results.length && (this.robot = results[0]);
  }

  async process(rawCommand: string): Promise<CommandResult> {
    const commandResult = this.commandProcessor.dispatch(
      rawCommand,
      this.board,
      this.robot
    );

    await this.save(rawCommand, commandResult);
    return commandResult;
  }

  async save(rawCommand: string, result: CommandResult) {
    const commandLog = new CommandLogs();
    commandLog.rawCommand = rawCommand;
    commandLog.valid = result.success;

    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.startTransaction();
    try {
      await queryRunner.manager.save(commandLog);
      let robot = result.robot;
      if (result.success) {
        robot!.version += 1;
        robot!.commandLog = commandLog;
        await queryRunner.manager.save(robot);
      }

      await queryRunner.commitTransaction();
      this.robot = robot;
    } catch (err) {
      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }
  }
}
