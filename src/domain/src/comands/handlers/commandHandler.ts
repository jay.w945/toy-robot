import { Board } from "../../models/board.model";
import { Command, CommandResult } from "../../models/command.model";
import { Robot } from "../../models/entities/robot.entity";
import { InvalidCommand } from "../../models/invalidCommand.model";

export abstract class CommandHandler<T extends Command> {
  abstract parse(rawCommand: string): T | null;

  abstract isValid(command: T, board: Board, robot: Robot | null): boolean;

  abstract execute(command: T, board: Board, robot: Robot | null): Robot | null;

  procecss(
    rawCommand: string,
    board: Board,
    robot: Robot | null
  ): CommandResult {
    let invalidCommand = new InvalidCommand();
    let currentRobot = robot == null ? null : robot.clone();
    const command = this.parse(rawCommand);
    if (!command) {
      return new CommandResult(invalidCommand, currentRobot);
    }

    if (!this.isValid(command, board, currentRobot)) {
      return new CommandResult(invalidCommand, currentRobot);
    }

    let resultRobot = this.execute(command, board, currentRobot);
    return new CommandResult(command, resultRobot);
  }
}
