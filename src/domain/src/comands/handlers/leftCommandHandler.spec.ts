import { CommandType } from "../../models/command.model";
import { LeftCommandHandler } from "./leftCommandHandler";

describe("Verify 'Left' command parsing functionality", () => {
  const commandHandler = new LeftCommandHandler();

  it("'Left' command parse valid raw command, should return command", async () => {
    const rawCommand = "LEFT";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand?.type).toEqual(CommandType.Left);
  });

  it("'Left' command parse valid lower case raw command, should return command", async () => {
    const rawCommand = "left";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand?.type).toEqual(CommandType.Left);
  });

  it("'Left' command parse invalid raw command, should fail", async () => {
    const rawCommand = "lef";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand).toBeNull();
  });
});
