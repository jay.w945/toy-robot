import { Board } from "../../models/board.model";
import { CommandType } from "../../models/command.model";
import { LeftCommand } from "../../models/leftCommand.model";
import { Robot } from "../../models/entities/robot.entity";
import { CommandHandler } from "./commandHandler";

export class LeftCommandHandler extends CommandHandler<LeftCommand> {
  parse(rawCommand: string): LeftCommand | null {
    const value = rawCommand.trim().toUpperCase();
    if (value !== CommandType.Left) {
      return null;
    }

    return new LeftCommand();
  }

  isValid(command: LeftCommand, board: Board, robot: Robot | null): boolean {
    return robot !== null;
  }

  execute(
    command: LeftCommand,
    board: Board,
    robot: Robot | null
  ): Robot | null {
    if (!this.isValid(command, board, robot)) {
      return robot;
    }

    robot!.turnLeft();
    return robot;
  }
}
