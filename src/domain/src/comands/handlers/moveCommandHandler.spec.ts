import { CommandType } from "../../models/command.model";
import { MoveCommandHandler } from "./moveCommandHandler";

describe("Verify 'Move' command parsing functionality", () => {
  const commandHandler = new MoveCommandHandler();

  it("'Move' command parse valid raw command, should return command", async () => {
    const rawCommand = "MOVE";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand?.type).toEqual(CommandType.Move);
  });

  it("'Move' command parse valid lower case raw command, should return command", async () => {
    const rawCommand = "move";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand?.type).toEqual(CommandType.Move);
  });

  it("'Move' command parse invalid raw command, should fail", async () => {
    const rawCommand = "mov";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand).toBeNull();
  });
});
