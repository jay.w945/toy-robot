import { Board } from "../../models/board.model";
import { CommandType } from "../../models/command.model";
import { MoveCommand } from "../../models/moveCommand.model";
import { Robot } from "../../models/entities/robot.entity";
import { CommandHandler } from "./commandHandler";

export class MoveCommandHandler extends CommandHandler<MoveCommand> {
  parse(rawCommand: string): MoveCommand | null {
    const value = rawCommand.trim().toUpperCase();
    if (value !== CommandType.Move) {
      return null;
    }

    return new MoveCommand();
  }

  isValid(command: MoveCommand, board: Board, robot: Robot | null): boolean {
    if (robot == null) {
      return false;
    }

    const newPosition = robot.tryMove();
    return board.isOnBoard(newPosition);
  }

  execute(
    command: MoveCommand,
    board: Board,
    robot: Robot | null
  ): Robot | null {
    if (!this.isValid(command, board, robot)) {
      return robot;
    }

    const newPosition = robot!.tryMove();
    robot!.moveTo(newPosition);
    return robot;
  }
}
