import { CommandType } from "../../models/command.model";
import { Direction } from "../../models/direction.model";
import { PlaceCommandHandler } from "./placeCommandHandler";

describe("Verify 'Place' command parsing functionality", () => {
  const commandHandler = new PlaceCommandHandler();

  it("'Place' command parse valid raw command, should return command", async () => {
    const rawCommand = "PLACE 2,3,NORTH";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand?.type).toEqual(CommandType.Place);
    expect(parsedCommmand?.positionX).toEqual(2);
    expect(parsedCommmand?.positionY).toEqual(3);
    expect(parsedCommmand?.direction).toEqual(Direction.NORTH);
  });

  it("'Place' command parse valid lower case raw command, should return command", async () => {
    const rawCommand = "place 2,3,north";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand?.type).toEqual(CommandType.Place);
  });

  it("'Place' command parse invalid raw command with wrong value, should fail", async () => {
    const rawCommand = "PLAC 2,3,NORTH";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand).toBeNull();
  });

  it("'Place' command parse invalid raw command with shorter value, should fail", async () => {
    const rawCommand = "place 1,NORTH";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand).toBeNull();
  });

  it("'Place' command parse invalid raw command with longer value, should fail", async () => {
    const rawCommand = "place 1,0,0,NORTH";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand).toBeNull();
  });

  it("'Place' command parse invalid raw command with invalid x, should fail", async () => {
    const rawCommand = "place x,0,NORTH";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand).toBeNull();
  });

  it("'Place' command parse invalid raw command with negative x, should fail", async () => {
    const rawCommand = "place -1,0,NORTH";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand).toBeNull();
  });

  it("'Place' command parse invalid raw command with invalid y, should fail", async () => {
    const rawCommand = "place 0,y,NORTH";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand).toBeNull();
  });

  it("'Place' command parse invalid raw command with negative y, should fail", async () => {
    const rawCommand = "place 0,-1,NORTH";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand).toBeNull();
  });

  it("'Place' command parse invalid raw command with invalid direction, should fail", async () => {
    const rawCommand = "place 0,0,NORT";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand).toBeNull();
  });
});
