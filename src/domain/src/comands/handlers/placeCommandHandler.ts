import { Board } from "../../models/board.model";
import { CommandType } from "../../models/command.model";
import { PlaceCommand } from "../../models/placeCommand.model";
import { Direction } from "../../models/direction.model";
import { Robot } from "../../models/entities/robot.entity";
import { Position } from "../../models/position.model";
import { CommandHandler } from "./commandHandler";

export class PlaceCommandHandler extends CommandHandler<PlaceCommand> {
  parse(rawCommand: string): PlaceCommand | null {
    const parts = rawCommand
      .trim()
      .toUpperCase()
      .split(/[\s|,]/)
      .filter((v) => v);

    if (
      parts.length !== 4 ||
      parts[0] !== CommandType.Place ||
      !this.isVaildNumber(parts[1]) ||
      !this.isVaildNumber(parts[2]) ||
      !(parts[3] in Direction)
    ) {
      return null;
    }

    const placeCommand = new PlaceCommand();
    placeCommand.type = CommandType.Place;
    placeCommand.positionX = parseInt(parts[1], 10);
    placeCommand.positionY = parseInt(parts[2], 10);
    placeCommand.direction = parts[3] as Direction;

    return placeCommand;
  }

  isVaildNumber(value: string): boolean {
    return /^\d+$/.test(value);
  }

  isValid(command: PlaceCommand, board: Board, robot: Robot | null): boolean {
    const newPosition = new Position(command.positionX, command.positionY);
    return board.isOnBoard(newPosition);
  }

  execute(
    command: PlaceCommand,
    board: Board,
    robot: Robot | null
  ): Robot | null {
    if (!this.isValid(command, board, robot)) {
      return robot;
    }

    robot = robot || new Robot();
    robot.direction = command.direction;
    robot.positionX = command.positionX;
    robot.positionY = command.positionY;

    return robot;
  }
}
