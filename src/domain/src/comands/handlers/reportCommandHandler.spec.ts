import { CommandType } from "../../models/command.model";
import { ReportCommandHandler } from "./reportCommandHandler";

describe("Verify 'Report' command parsing functionality", () => {
  const commandHandler = new ReportCommandHandler();

  it("'Report' command parse valid raw command, should return command", async () => {
    const rawCommand = "REPORT";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand?.type).toEqual(CommandType.Report);
  });

  it("'Report' command parse valid lower case raw command, should return command", async () => {
    const rawCommand = "report";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand?.type).toEqual(CommandType.Report);
  });

  it("'Report' command parse invalid raw command, should fail", async () => {
    const rawCommand = "repor";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand).toBeNull();
  });
});
