import { Board } from "../../models/board.model";
import { CommandType } from "../../models/command.model";
import { ReportCommand } from "../../models/reportCommand.model";
import { Robot } from "../../models/entities/robot.entity";
import { CommandHandler } from "./commandHandler";

export class ReportCommandHandler extends CommandHandler<ReportCommand> {
  parse(rawCommand: string): ReportCommand | null {
    const value = rawCommand.trim().toUpperCase();
    if (value !== CommandType.Report) {
      return null;
    }

    return new ReportCommand();
  }

  isValid(command: ReportCommand, board: Board, robot: Robot | null): boolean {
    return robot !== null;
  }

  execute(
    command: ReportCommand,
    board: Board,
    robot: Robot | null
  ): Robot | null {
    return robot;
  }
}
