import { CommandType } from "../../models/command.model";
import { RightCommandHandler } from "./rightCommandHandler";

describe("Verify 'Right' command parsing functionality", () => {
  const commandHandler = new RightCommandHandler();

  it("'Right' command parse valid raw command, should return command", async () => {
    const rawCommand = "RIGHT";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand?.type).toEqual(CommandType.Right);
  });

  it("'Right' command parse valid lower case raw command, should return command", async () => {
    const rawCommand = "right";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand?.type).toEqual(CommandType.Right);
  });

  it("'Right' command parse invalid raw command, should fail", async () => {
    const rawCommand = "righ";

    const parsedCommmand = commandHandler.parse(rawCommand);

    expect(parsedCommmand).toBeNull();
  });
});
