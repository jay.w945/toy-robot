import { Board } from "../../models/board.model";
import { CommandType } from "../../models/command.model";
import { RightCommand } from "../../models/rightCommand.model";
import { Robot } from "../../models/entities/robot.entity";
import { CommandHandler } from "./commandHandler";

export class RightCommandHandler extends CommandHandler<RightCommand> {
  parse(rawCommand: string): RightCommand | null {
    const value = rawCommand.trim().toUpperCase();
    if (value !== CommandType.Right) {
      return null;
    }

    return new RightCommand();
  }

  isValid(command: RightCommand, board: Board, robot: Robot | null): boolean {
    return robot !== null;
  }

  execute(
    command: RightCommand,
    board: Board,
    robot: Robot | null
  ): Robot | null {
    if (!this.isValid(command, board, robot)) {
      return robot;
    }

    robot!.turnRight();
    return robot;
  }
}
