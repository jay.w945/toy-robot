import { Injectable } from "@nestjs/common";
import { DataSource } from "typeorm";

import { Config } from "../models/common/config";

import { CommandLogs } from "../models/entities/commandLogs.entity";
import { Robot } from "../models/entities/robot.entity";

export const DATA_SOURCE = "DATA_SOURCE";

@Injectable()
export class DataSourceProvider {
  private dataSource: DataSource;
  private initialized: boolean = false;
  constructor(private appConfig: Config) {
    const appDbConfig = this.appConfig.db || this.appConfig;
    const dbConfig = {
      ...appDbConfig,
      entities: [CommandLogs, Robot],
    } as any;

    this.dataSource = new DataSource(dbConfig);
  }

  async init() {
    if (!this.initialized) {
      await this.dataSource.initialize();
      this.initialized = true;
    }
  }

  getAppDataSource(): DataSource {
    return this.dataSource;
  }
}
