import { Field, ObjectType } from "@nestjs/graphql";
import { Position } from "./position.model";

export const GAME_BOARD = "GAME_BOARD";

@ObjectType({ description: "board" })
export class Board {
  @Field()
  xSize: number;
  @Field()
  ySize: number;

  constructor(xSize: number, ySize: number) {
    this.xSize = xSize;
    this.ySize = ySize;
  }

  isOnBoard(position: Position): boolean {
    return (
      position.x >= 0 &&
      position.x < this.xSize &&
      position.y >= 0 &&
      position.y < this.ySize
    );
  }
}
