import { Field, ObjectType } from "@nestjs/graphql";
import { Robot } from "./entities/robot.entity";

export enum CommandType {
  Place = "PLACE",
  Move = "MOVE",
  Left = "LEFT",
  Right = "RIGHT",
  Report = "REPORT",
  Invalid = "INVALID",
}

export interface Command {
  type: CommandType;
}

@ObjectType({ description: "command_result" })
export class CommandResult {
  constructor(command: Command, robot: Robot | null) {
    this.command = command;
    this.robot = robot;
    this.success =
      this.command.type != CommandType.Invalid && this.robot != null;
  }

  command!: Command;

  @Field()
  robot!: Robot | null;

  @Field()
  success!: boolean;
}
