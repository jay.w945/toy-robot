export const APP_CONFIG = "APP_CONFIG";
export type Config = {
  [key: string]: any;
};
