import { CreateDateColumn, Entity, PrimaryColumn } from "typeorm";
import { Field, ID, ObjectType } from "@nestjs/graphql";
import ShortUniqueId from "short-unique-id";

@Entity()
@ObjectType({ isAbstract: true })
export class Base {
  @PrimaryColumn()
  @Field(() => ID)
  id: string = new ShortUniqueId()();

  @CreateDateColumn({ type: "timestamp" })
  @Field()
  timestamp?: Date;
}
