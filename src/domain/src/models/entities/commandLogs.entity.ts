import { Entity, Column } from "typeorm";
import { Field, ObjectType } from "@nestjs/graphql";

import { Base } from "./base.entity";

@Entity()
@ObjectType({ description: "commandLogs" })
export class CommandLogs extends Base {
  @Column({ name: "raw_command" })
  @Field()
  rawCommand!: string;

  @Column()
  @Field()
  valid: boolean = false;
}
