import { Direction } from "../direction.model";
import { Robot } from "./robot.entity";

describe("Verify Robot functionality", () => {
  const robot = new Robot();

  it("robot's diretions should always be ['EAST', 'SOUTH', 'WEST', 'NORTH']", async () => {
    expect(robot.directions).toEqual(["EAST", "SOUTH", "WEST", "NORTH"]);
  });

  const turnLeftTheory = [
    { current: Direction.EAST, newDirection: Direction.NORTH },
    { current: Direction.NORTH, newDirection: Direction.WEST },
    { current: Direction.WEST, newDirection: Direction.SOUTH },
    { current: Direction.SOUTH, newDirection: Direction.EAST },
  ];
  it.each(turnLeftTheory)(
    "Robot turn left should work",
    ({ current, newDirection }) => {
      robot.direction = current;

      robot.turnLeft();

      expect(robot.direction).toBe(newDirection);
    }
  );

  const turnRightTheory = [
    { current: Direction.EAST, newDirection: Direction.SOUTH },
    { current: Direction.NORTH, newDirection: Direction.EAST },
    { current: Direction.WEST, newDirection: Direction.NORTH },
    { current: Direction.SOUTH, newDirection: Direction.WEST },
  ];
  it.each(turnRightTheory)(
    "Robot turn right should work",
    ({ current, newDirection }) => {
      robot.direction = current;

      robot.turnRight();

      expect(robot.direction).toBe(newDirection);
    }
  );

  const tryMoveTheory = [
    {
      direction: Direction.EAST,
      positionX: 4,
      positionY: 4,
      newPositionX: 5,
      newPositionY: 4,
    },
    {
      direction: Direction.SOUTH,
      positionX: 0,
      positionY: 0,
      newPositionX: 0,
      newPositionY: -1,
    },
    {
      direction: Direction.WEST,
      positionX: 0,
      positionY: 0,
      newPositionX: -1,
      newPositionY: 0,
    },
    {
      direction: Direction.NORTH,
      positionX: 4,
      positionY: 4,
      newPositionX: 4,
      newPositionY: 5,
    },
  ];
  it.each(tryMoveTheory)(
    "Robot try move should always return assume position even if it's NOT on the board",
    ({ direction, positionX, positionY, newPositionX, newPositionY }) => {
      robot.direction = direction;
      robot.positionX = positionX;
      robot.positionY = positionY;

      const newPosition = robot.tryMove();

      expect(newPosition.x).toBe(newPositionX);
      expect(newPosition.y).toBe(newPositionY);
    }
  );
});
