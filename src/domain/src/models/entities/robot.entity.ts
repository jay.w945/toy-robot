import {
  Entity,
  Column,
  Unique,
  PrimaryColumn,
  OneToOne,
  JoinColumn,
} from "typeorm";
import { Field, ObjectType } from "@nestjs/graphql";

import { Base } from "./base.entity";
import { Direction } from "../direction.model";
import { Position } from "../position.model";
import { CommandLogs } from "./commandLogs.entity";

@Entity()
@ObjectType({ description: "robot" })
export class Robot extends Base {
  @PrimaryColumn("int")
  @Field()
  version: number = 0;

  @Column({ type: String })
  @Field()
  direction!: Direction;

  @Column()
  @Field()
  positionX!: number;

  @Column()
  @Field()
  positionY!: number;

  @OneToOne(() => CommandLogs)
  @JoinColumn()
  commandLog!: CommandLogs;

  directions: string[] = Object.keys(Direction);

  turnLeft() {
    let directionIndex = this.directions.indexOf(this.direction);

    directionIndex > 0 &&
      (this.direction = this.directions[directionIndex - 1] as Direction);

    directionIndex === 0 &&
      (this.direction = this.directions[
        this.directions.length - 1
      ] as Direction);
  }

  turnRight() {
    let directionIndex = this.directions.indexOf(this.direction);

    directionIndex < this.directions.length - 1 &&
      (this.direction = this.directions[directionIndex + 1] as Direction);

    directionIndex === this.directions.length - 1 &&
      (this.direction = this.directions[0] as Direction);
  }

  currentPosition(): Position {
    return new Position(this.positionX, this.positionY);
  }

  moveTo(position: Position) {
    this.positionX = position.x;
    this.positionY = position.y;
  }

  tryMove(): Position {
    let newPosition = new Position(this.positionX, this.positionY);

    this.direction === Direction.EAST && (newPosition.x += 1);
    this.direction === Direction.SOUTH && (newPosition.y -= 1);
    this.direction === Direction.WEST && (newPosition.x -= 1);
    this.direction === Direction.NORTH && (newPosition.y += 1);

    return newPosition;
  }

  clone(): Robot {
    const newRobot = new Robot();
    newRobot.id = this.id;
    newRobot.direction = this.direction;
    newRobot.positionX = this.positionX;
    newRobot.positionY = this.positionY;
    newRobot.version = this.version;

    return newRobot;
  }
}
