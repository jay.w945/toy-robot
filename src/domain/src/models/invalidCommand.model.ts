import { Command, CommandType } from "./command.model";

export class InvalidCommand implements Command {
  type: CommandType = CommandType.Invalid;
}
