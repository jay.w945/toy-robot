import { Command, CommandType } from "./command.model";

export class LeftCommand implements Command {
  type: CommandType = CommandType.Left;
}
