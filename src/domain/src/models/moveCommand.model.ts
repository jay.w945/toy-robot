import { Command, CommandType } from "./command.model";

export class MoveCommand implements Command {
  type: CommandType = CommandType.Move;
}
