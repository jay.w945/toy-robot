import { Direction } from "./direction.model";
import { Command, CommandType } from "./command.model";

export class PlaceCommand implements Command {
  type: CommandType = CommandType.Place;
  positionX!: number;
  positionY!: number;
  direction!: Direction;
}
