import { Command, CommandType } from "./command.model";

export class ReportCommand implements Command {
  type: CommandType = CommandType.Report;
}
