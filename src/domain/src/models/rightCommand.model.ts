import { Command, CommandType } from "./command.model";

export class RightCommand implements Command {
  type: CommandType = CommandType.Right;
}
