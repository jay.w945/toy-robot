import { Inject, Injectable } from "@nestjs/common";
import { DataSource, FindManyOptions, Repository } from "typeorm";

import { DATA_SOURCE } from "../data/data-source.provider";
import { Robot } from "../models/entities/robot.entity";

@Injectable()
export class RobotQuery {
  private repo: Repository<Robot>;

  constructor(@Inject(DATA_SOURCE) private dataSource: DataSource) {
    this.repo = this.dataSource.getRepository(Robot);
  }

  async getRobot(id: string | null): Promise<Robot | null> {
    const query = {
      order: { timestamp: "DESC" },
      take: 1,
    } as FindManyOptions<Robot>;

    id && (query.where = { id: id });
    const results = await this.repo.find(query);

    return results.length ? results[0] : null;
  }
}
