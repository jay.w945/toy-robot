import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import { Robot } from "../generated";
import { Board } from "./board.component";

describe("testing board component", () => {
  it("Board with valid size, should render board ", async () => {
    const { container } = render(<Board xSize={3} ySize={4} />);

    expect(container.querySelector("table")).toBeInTheDocument();
    expect(container.querySelectorAll("td").length).toEqual(12);
  });

  it("Board with invalid size, should not render board", async () => {
    const { container } = render(<Board />);

    expect(container.querySelector("table")).toBeInTheDocument();
    expect(container.querySelectorAll("td").length).toEqual(0);
  });

  it("Board with valid size and  robot, should render board and robot in the right place", async () => {
    const robot = {
      id: "testing robot",
      positionX: 2,
      positionY: 1,
      direction: "SOUTH",
    } as Robot;

    const expectRobotPosition = 8;

    const { container } = render(<Board xSize={3} ySize={4} robot={robot} />);

    const td = container.querySelectorAll("td")[expectRobotPosition];
    expect(td.querySelector("[data-testid='robot']")).toBeInTheDocument();
  });
});
