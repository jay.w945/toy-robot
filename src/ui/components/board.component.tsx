import { Robot } from "../generated";
import { ChevronDoubleDownIcon } from "@heroicons/react/solid";
import { ChevronDoubleUpIcon } from "@heroicons/react/solid";
import { ChevronDoubleLeftIcon } from "@heroicons/react/solid";
import { ChevronDoubleRightIcon } from "@heroicons/react/solid";

import { EAST, SOUTH, WEST, NORTH } from "../common/directions";
const directionIconMap: { [key: string]: Function } = {
  [EAST]: ChevronDoubleRightIcon,
  [SOUTH]: ChevronDoubleDownIcon,
  [WEST]: ChevronDoubleLeftIcon,
  [NORTH]: ChevronDoubleUpIcon,
};

export interface BoardProps {
  xSize?: number;
  ySize?: number;
  robot?: Robot;
}

export const Board = ({ xSize, ySize, robot }: BoardProps) => {
  const directionIcon = (direction: string) => {
    if (!directionIconMap[direction]) {
      return null;
    }

    const Icon = directionIconMap[direction];
    return <Icon data-testid="robot" className="h-18 w-18 text-blue-500" />;
  };

  return (
    <table>
      <tbody>
        {xSize &&
          ySize &&
          [...Array(ySize)].map((_, y) => (
            <tr key={y}>
              {[...Array(xSize)].map((_, x) => (
                <td
                  className="inline-block m-4 box-border h-24 w-24 p-4 border-4"
                  key={x}
                >
                  {robot &&
                    robot.positionX === x &&
                    robot.positionY == ySize! - 1 - y &&
                    directionIcon(robot.direction)}
                </td>
              ))}
            </tr>
          ))}
      </tbody>
    </table>
  );
};
