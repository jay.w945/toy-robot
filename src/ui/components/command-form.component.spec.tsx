import "@testing-library/jest-dom";
import {
  act,
  fireEvent,
  getByText,
  render,
  screen,
  waitFor,
} from "@testing-library/react";

import { CommandForm } from "./command-form.component";

describe("testing command-form component", () => {
  it("should render", async () => {
    const handleRunCommand = (command: string) => Promise.resolve(true);

    render(<CommandForm handleRunCommand={handleRunCommand} />);

    expect(screen.getByTestId("command-input")).toBeInTheDocument();
    expect(screen.getByTestId("submit-btn")).toBeInTheDocument();
  });

  it("run valid command should show success and clear input value", async () => {
    const handleRunCommand = (command: string) => Promise.resolve(true);

    const { container } = render(
      <CommandForm handleRunCommand={handleRunCommand} />
    );

    const input = container.querySelector("input");
    fireEvent.change(input!, { target: { value: "valid command" } });
    fireEvent(
      getByText(container, "Run"),
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    await waitFor(() => {
      expect(screen.getByTestId("success-alert")).toBeInTheDocument();
      expect(screen.getByTestId("command-input")).toHaveValue("");
    });
  });

  it("run invalid command should show invalid command and keep input value", async () => {
    const handleRunCommand = (command: string) => Promise.resolve(false);

    const { container } = render(
      <CommandForm handleRunCommand={handleRunCommand} />
    );

    const input = container.querySelector("input");
    fireEvent.change(input!, { target: { value: "invalid command" } });

    fireEvent(
      getByText(container, "Run"),
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    await waitFor(() => {
      expect(screen.getByTestId("invalid-alert")).toBeInTheDocument();
      expect(screen.getByTestId("command-input")).toHaveValue(
        "invalid command"
      );
    });
  });
});
