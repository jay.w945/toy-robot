import { FormEvent, useRef, useState } from "react";

export interface CommandFormProps {
  handleRunCommand: (command: string) => Promise<boolean>;
}

export const CommandForm = ({ handleRunCommand }: CommandFormProps) => {
  const [commandResult, setCommandResult] = useState<boolean | null>(null);
  const commandInputRef = useRef<HTMLInputElement>(null);
  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    if (commandInputRef.current) {
      handleRunCommand(commandInputRef.current?.value).then((success) => {
        setCommandResult(success);
        if (success && commandInputRef.current) {
          commandInputRef.current.value = "";
        }
      });
    }

    event.preventDefault();
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <div className="mb-4">
          <input
            className="shadow appearance-none border rounded mr-2 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="command"
            data-testid="command-input"
            type="text"
            placeholder="command"
            ref={commandInputRef}
          />
          <button
            className="inline bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            type="submit"
            data-testid="submit-btn"
          >
            Run
          </button>
        </div>
      </form>

      {commandResult === false && (
        <div
          className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
          role="alert"
          data-testid="invalid-alert"
        >
          <strong className="font-bold">Invalid command.</strong>
        </div>
      )}

      {commandResult === true && (
        <div
          className="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
          role="alert"
          data-testid="success-alert"
        >
          <strong className="font-bold">Success.</strong>
        </div>
      )}
    </>
  );
};
