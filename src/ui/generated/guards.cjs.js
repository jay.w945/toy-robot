
var Robot_possibleTypes = ['Robot']
module.exports.isRobot = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRobot"')
  return Robot_possibleTypes.includes(obj.__typename)
}



var Board_possibleTypes = ['Board']
module.exports.isBoard = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isBoard"')
  return Board_possibleTypes.includes(obj.__typename)
}



var CommandResult_possibleTypes = ['CommandResult']
module.exports.isCommandResult = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCommandResult"')
  return CommandResult_possibleTypes.includes(obj.__typename)
}



var Query_possibleTypes = ['Query']
module.exports.isQuery = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isQuery"')
  return Query_possibleTypes.includes(obj.__typename)
}



var Mutation_possibleTypes = ['Mutation']
module.exports.isMutation = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isMutation"')
  return Mutation_possibleTypes.includes(obj.__typename)
}
