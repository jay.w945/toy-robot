import {FieldsSelection,Observable} from '@genql/runtime'

export type Scalars = {
    ID: string,
    Float: number,
    String: string,
    DateTime: any,
    Boolean: boolean,
}


/** robot */
export interface Robot {
    id: Scalars['ID']
    timestamp: Scalars['DateTime']
    version: Scalars['Float']
    direction: Scalars['String']
    positionX: Scalars['Float']
    positionY: Scalars['Float']
    __typename: 'Robot'
}


/** board */
export interface Board {
    xSize: Scalars['Float']
    ySize: Scalars['Float']
    __typename: 'Board'
}


/** command_result */
export interface CommandResult {
    robot: Robot
    success: Scalars['Boolean']
    __typename: 'CommandResult'
}

export interface Query {
    robot?: Robot
    board: Board
    __typename: 'Query'
}

export interface Mutation {
    runCommand: CommandResult
    __typename: 'Mutation'
}


/** robot */
export interface RobotRequest{
    id?: boolean | number
    timestamp?: boolean | number
    version?: boolean | number
    direction?: boolean | number
    positionX?: boolean | number
    positionY?: boolean | number
    __typename?: boolean | number
    __scalar?: boolean | number
}


/** board */
export interface BoardRequest{
    xSize?: boolean | number
    ySize?: boolean | number
    __typename?: boolean | number
    __scalar?: boolean | number
}


/** command_result */
export interface CommandResultRequest{
    robot?: RobotRequest
    success?: boolean | number
    __typename?: boolean | number
    __scalar?: boolean | number
}

export interface QueryRequest{
    robot?: [{id?: (Scalars['String'] | null)},RobotRequest] | RobotRequest
    board?: BoardRequest
    __typename?: boolean | number
    __scalar?: boolean | number
}

export interface MutationRequest{
    runCommand?: [{command: Scalars['String']},CommandResultRequest]
    __typename?: boolean | number
    __scalar?: boolean | number
}


const Robot_possibleTypes: string[] = ['Robot']
export const isRobot = (obj?: { __typename?: any } | null): obj is Robot => {
  if (!obj?.__typename) throw new Error('__typename is missing in "isRobot"')
  return Robot_possibleTypes.includes(obj.__typename)
}



const Board_possibleTypes: string[] = ['Board']
export const isBoard = (obj?: { __typename?: any } | null): obj is Board => {
  if (!obj?.__typename) throw new Error('__typename is missing in "isBoard"')
  return Board_possibleTypes.includes(obj.__typename)
}



const CommandResult_possibleTypes: string[] = ['CommandResult']
export const isCommandResult = (obj?: { __typename?: any } | null): obj is CommandResult => {
  if (!obj?.__typename) throw new Error('__typename is missing in "isCommandResult"')
  return CommandResult_possibleTypes.includes(obj.__typename)
}



const Query_possibleTypes: string[] = ['Query']
export const isQuery = (obj?: { __typename?: any } | null): obj is Query => {
  if (!obj?.__typename) throw new Error('__typename is missing in "isQuery"')
  return Query_possibleTypes.includes(obj.__typename)
}



const Mutation_possibleTypes: string[] = ['Mutation']
export const isMutation = (obj?: { __typename?: any } | null): obj is Mutation => {
  if (!obj?.__typename) throw new Error('__typename is missing in "isMutation"')
  return Mutation_possibleTypes.includes(obj.__typename)
}



/** robot */
export interface RobotPromiseChain{
    id: ({get: (request?: boolean|number, defaultValue?: Scalars['ID']) => Promise<Scalars['ID']>}),
    timestamp: ({get: (request?: boolean|number, defaultValue?: Scalars['DateTime']) => Promise<Scalars['DateTime']>}),
    version: ({get: (request?: boolean|number, defaultValue?: Scalars['Float']) => Promise<Scalars['Float']>}),
    direction: ({get: (request?: boolean|number, defaultValue?: Scalars['String']) => Promise<Scalars['String']>}),
    positionX: ({get: (request?: boolean|number, defaultValue?: Scalars['Float']) => Promise<Scalars['Float']>}),
    positionY: ({get: (request?: boolean|number, defaultValue?: Scalars['Float']) => Promise<Scalars['Float']>})
}


/** robot */
export interface RobotObservableChain{
    id: ({get: (request?: boolean|number, defaultValue?: Scalars['ID']) => Observable<Scalars['ID']>}),
    timestamp: ({get: (request?: boolean|number, defaultValue?: Scalars['DateTime']) => Observable<Scalars['DateTime']>}),
    version: ({get: (request?: boolean|number, defaultValue?: Scalars['Float']) => Observable<Scalars['Float']>}),
    direction: ({get: (request?: boolean|number, defaultValue?: Scalars['String']) => Observable<Scalars['String']>}),
    positionX: ({get: (request?: boolean|number, defaultValue?: Scalars['Float']) => Observable<Scalars['Float']>}),
    positionY: ({get: (request?: boolean|number, defaultValue?: Scalars['Float']) => Observable<Scalars['Float']>})
}


/** board */
export interface BoardPromiseChain{
    xSize: ({get: (request?: boolean|number, defaultValue?: Scalars['Float']) => Promise<Scalars['Float']>}),
    ySize: ({get: (request?: boolean|number, defaultValue?: Scalars['Float']) => Promise<Scalars['Float']>})
}


/** board */
export interface BoardObservableChain{
    xSize: ({get: (request?: boolean|number, defaultValue?: Scalars['Float']) => Observable<Scalars['Float']>}),
    ySize: ({get: (request?: boolean|number, defaultValue?: Scalars['Float']) => Observable<Scalars['Float']>})
}


/** command_result */
export interface CommandResultPromiseChain{
    robot: (RobotPromiseChain & {get: <R extends RobotRequest>(request: R, defaultValue?: FieldsSelection<Robot, R>) => Promise<FieldsSelection<Robot, R>>}),
    success: ({get: (request?: boolean|number, defaultValue?: Scalars['Boolean']) => Promise<Scalars['Boolean']>})
}


/** command_result */
export interface CommandResultObservableChain{
    robot: (RobotObservableChain & {get: <R extends RobotRequest>(request: R, defaultValue?: FieldsSelection<Robot, R>) => Observable<FieldsSelection<Robot, R>>}),
    success: ({get: (request?: boolean|number, defaultValue?: Scalars['Boolean']) => Observable<Scalars['Boolean']>})
}

export interface QueryPromiseChain{
    robot: ((args?: {id?: (Scalars['String'] | null)}) => RobotPromiseChain & {get: <R extends RobotRequest>(request: R, defaultValue?: (FieldsSelection<Robot, R> | undefined)) => Promise<(FieldsSelection<Robot, R> | undefined)>})&(RobotPromiseChain & {get: <R extends RobotRequest>(request: R, defaultValue?: (FieldsSelection<Robot, R> | undefined)) => Promise<(FieldsSelection<Robot, R> | undefined)>}),
    board: (BoardPromiseChain & {get: <R extends BoardRequest>(request: R, defaultValue?: FieldsSelection<Board, R>) => Promise<FieldsSelection<Board, R>>})
}

export interface QueryObservableChain{
    robot: ((args?: {id?: (Scalars['String'] | null)}) => RobotObservableChain & {get: <R extends RobotRequest>(request: R, defaultValue?: (FieldsSelection<Robot, R> | undefined)) => Observable<(FieldsSelection<Robot, R> | undefined)>})&(RobotObservableChain & {get: <R extends RobotRequest>(request: R, defaultValue?: (FieldsSelection<Robot, R> | undefined)) => Observable<(FieldsSelection<Robot, R> | undefined)>}),
    board: (BoardObservableChain & {get: <R extends BoardRequest>(request: R, defaultValue?: FieldsSelection<Board, R>) => Observable<FieldsSelection<Board, R>>})
}

export interface MutationPromiseChain{
    runCommand: ((args: {command: Scalars['String']}) => CommandResultPromiseChain & {get: <R extends CommandResultRequest>(request: R, defaultValue?: FieldsSelection<CommandResult, R>) => Promise<FieldsSelection<CommandResult, R>>})
}

export interface MutationObservableChain{
    runCommand: ((args: {command: Scalars['String']}) => CommandResultObservableChain & {get: <R extends CommandResultRequest>(request: R, defaultValue?: FieldsSelection<CommandResult, R>) => Observable<FieldsSelection<CommandResult, R>>})
}