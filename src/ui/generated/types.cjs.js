module.exports = {
    "scalars": [
        1,
        2,
        3,
        4,
        7
    ],
    "types": {
        "Robot": {
            "id": [
                1
            ],
            "timestamp": [
                4
            ],
            "version": [
                2
            ],
            "direction": [
                3
            ],
            "positionX": [
                2
            ],
            "positionY": [
                2
            ],
            "__typename": [
                3
            ]
        },
        "ID": {},
        "Float": {},
        "String": {},
        "DateTime": {},
        "Board": {
            "xSize": [
                2
            ],
            "ySize": [
                2
            ],
            "__typename": [
                3
            ]
        },
        "CommandResult": {
            "robot": [
                0
            ],
            "success": [
                7
            ],
            "__typename": [
                3
            ]
        },
        "Boolean": {},
        "Query": {
            "robot": [
                0,
                {
                    "id": [
                        3
                    ]
                }
            ],
            "board": [
                5
            ],
            "__typename": [
                3
            ]
        },
        "Mutation": {
            "runCommand": [
                6,
                {
                    "command": [
                        3,
                        "String!"
                    ]
                }
            ],
            "__typename": [
                3
            ]
        }
    }
}