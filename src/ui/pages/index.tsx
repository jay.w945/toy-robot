import { NextPage } from "next";
import Head from "next/head";
import styles from "../styles/Home.module.css";
import React, { useState } from "react";
import useSWR from "swr";
import { createClient, Robot } from "../generated";
import { Board } from "../components/board.component";
import { CommandForm } from "../components/command-form.component";

const client = createClient({ url: "http://localhost:5100/graphql" });
const querier = () =>
  client.query({
    robot: {
      id: true,
      direction: true,
      positionX: true,
      positionY: true,
    },
    board: {
      xSize: true,
      ySize: true,
    },
  });

const Home: NextPage = () => {
  const { data, error, mutate } = useSWR("query", querier);

  const handleRunCommand = async (command: string): Promise<boolean> => {
    const result = await client.mutation({
      runCommand: [{ command: command }, { success: true }],
    });
    mutate();

    return result.runCommand.success;
  };

  const board = data?.board;
  const robot = data?.robot as Robot;
  return (
    <div className={styles.container}>
      <Head>
        <title>Toy Robot</title>
        <meta name="description" content="Toy Robot" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <CommandForm handleRunCommand={handleRunCommand} />
        <Board {...board} robot={robot} />
      </main>
    </div>
  );
};

export default Home;
